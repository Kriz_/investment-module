#%%
import numpy as np
import matplotlib.pyplot as plt

"""
Apdrošināšanas rules: 1% GPL, iekasēts mēnesī, 20% Atmaksa
"""

income_tax = 0.2

def index_fund(inital_capital,time,fee,fee_periode,function,return_samples):
    memory = np.zeros((3,time//return_samples+1))
    inital_capital-=fee*inital_capital
    memory[2,0]=inital_capital
    memory[1,0]=function[0]
    for i in range(1,time):
        if i%fee_periode==0:
            increase = (function[i]-function[i-fee_periode])/function[i-fee_periode]
            inital_capital+=increase*inital_capital
            inital_capital-=fee*inital_capital
        if i%return_samples==0:
            memory[0,i//return_samples]=i
            memory[1,i//return_samples]=function[i]
            memory[2,i//return_samples]=inital_capital
    return(memory)


function = np.linspace(200, 1200,365*10)
for i in range(1,365*10):
    function[i]=function[i-1]+function[i-1]*0.10/365

mana_nauda = 4000

dabusim_atpakal = income_tax*mana_nauda
dati = index_fund(mana_nauda,365*10,0.01/12,30,function,60)
dati_2 = index_fund(mana_nauda,365*10,0.005,365,function,60)
dati_3 = index_fund(dabusim_atpakal,365*10,0.005,365,function,60)


plt.plot(dati[0],dati[2]+dati_3[2],label="Tava Naudaun apdrošināšana")
plt.plot(dati_2[0],dati_2[2],label="Tava Nauda")
#plt.plot(dati_2[0],dati_2[1],label="Tava Nauda_2")
plt.grid()
plt.legend()


# %%
